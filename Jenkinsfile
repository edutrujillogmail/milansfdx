#!groovy

import groovy.json.JsonSlurperClassic
import groovy.json.JsonOutput	
import jenkins.model.*

node {

	echo "The build PROJECT is ${env.PROJECT}"
	echo "The build DEPLOY is ${env.DEPLOY}"
	echo "The build MODE is ${env.MODE}"
	echo "The build RUN TESTS ON DEPLOY is ${env.RUNTESTSDEPLOY}"
	echo "The build BRANCH is ${env.BRANCH}"
	echo "The build ENVIROMENT is ${env.ENVIROMENT}"
	echo "The build QA on CODE is ${env.QACODE}"
	echo "The build ADHERENCE is ${env.ADHERENCE}"
	echo "The build ADMIN USER is admin${env.PROJECT}@${env.ENVIROMENT}.com"

	echo "The build JOB is ${env.JOB_NAME} / ${env.BUILD_NUMBER}"

	LOCALRUNTESTS = ''
	SONARURL = 'http://40.69.74.208:9000'
	SONARLOGIN = '7a7c59d5df622a415f8e3138955adf09efe3d6ed'
	if (env.RUNTESTSDEPLOY == 'true'){
		LOCALRUNTESTS = '-l RunLocalTests';
	}

	withCredentials([file(credentialsId: "${env.PROJECT}_${env.ENVIROMENT}_SERVERKEY", variable: 'SERVERKEY'),
					string(credentialsId: "${env.PROJECT}_${env.ENVIROMENT}_CONSUMERKEY", variable: 'CONSUMERKEY'),
					string(credentialsId: "${env.PROJECT}_${env.ENVIROMENT}_CONSUMERSECRET", variable: 'CONSUMERSECRET')
	]) {

		stage('Checkout & Pull') {
			checkout scm	
		}

		stage('Login') {
			if (env.ENVIROMENT != 'PRO') {
				sh "/usr/local/bin/sfdx force:auth:jwt:grant --clientid ${CONSUMERKEY} --username admin${env.PROJECT}@${env.ENVIROMENT}.com --jwtkeyfile ${SERVERKEY} -a ${env.ENVIROMENT}${env.PROJECT} -r https://test.salesforce.com"
			} else {
				sh "/usr/local/bin/sfdx force:auth:jwt:grant --clientid ${CONSUMERKEY} --username admin${env.PROJECT}@${env.ENVIROMENT}.com --jwtkeyfile ${SERVERKEY} -a ${env.ENVIROMENT}${env.PROJECT} -r https://login.salesforce.com"
			}
		}

		if (env.ADHERENCE == 'true') {
			stage('Generating Adherence') {
					echo "set DISPLAY"
					sh "export DISPLAY=:0.0"
					echo "converting"
					sh "/usr/local/bin/sfdx force:source:convert -d metadataadherence/ -r force-app"
					echo "executing"
					sh "/usr/local/bin/ExecuteAdherence.sh -path metadataadherence"
					echo "moving"
					sh "cp -a /usr/local/lib/Adherencia/result/ResultAdherence.xlsx result"
					echo "done"
			}   
		}

		if (env.QACODE == 'true') {
			try {
				stage('Pmd Static Code Analysis') {
						sh "/usr/local/bin/run.sh pmd -d force-app/main/default/classes -f xml -l apex -R rulesets/apex/quickstart.xml > pmd_result.xml"
				}
			} catch (Exception e) {
				echo "We found PMD APEX Errors, but we continue"  
			}
			try {
				stage('Config nodeJS modules') {
					// Reuiqred to install "npm install -g eslint"
					sh "npm install eslint-config-standard --save-dev"
					sh "npm install babel-eslint --save-dev"
					sh "npm install eslint-plugin-wc --save-dev"
					sh "npm install @salesforce/eslint-plugin-aura@latest --save-dev"
				}
			} catch (Exception e) {
				echo "We cant config nodeJS modules"  
			}
			try {			
				stage('Lightning & WebComponents ESLint Analysis') {
						sh "eslint . -oeslint_result -f checkstyle" 
				}
			} catch (Exception e) {
				echo "We found WebComponent ESLint Errors, but we continue"  
			}
		} 

		if (env.DEPLOY == 'deploy') {
			if (env.MODE == 'source') {
				stage('Source Deployment') {
						sh "/usr/local/bin/sfdx force:source:deploy -w 50 ${LOCALRUNTESTS} -u ${env.ENVIROMENT}${env.PROJECT} -p force-app/main/default/"
				}
			} else if (env.MODE == 'mdapi') {
				stage('MDAPI Deployment') {
						sh "/usr/local/bin/sfdx force:mdapi:deploy --deploydir metadata -w 50 ${LOCALRUNTESTS} -u ${env.ENVIROMENT}${env.PROJECT}"
				}
			} else if (env.MODE == 'manifest') {
				stage('Source Deployment') {
						sh "/usr/local/bin/sfdx force:source:deploy -w 50 ${LOCALRUNTESTS} -u ${env.ENVIROMENT}${env.PROJECT} -p force-app/main/default/ -x manifest/package.xml"
				}
			}
		} else {
			if (env.MODE == 'source') {
				stage('Source Validation') {
						sh "/usr/local/bin/sfdx force:source:deploy -c -w 50 ${LOCALRUNTESTS} -u ${env.ENVIROMENT}${env.PROJECT} -p force-app/main/default/"
				}
			} else if (env.MODE == 'mdapi') {
				stage('MDAPI Validation') {
						sh "/usr/local/bin/sfdx force:mdapi:deploy -c --deploydir metadata -w 50 ${LOCALRUNTESTS} -u ${env.ENVIROMENT}${env.PROJECT}"
				}
			} else if (env.MODE == 'manifest') {
				stage('Source Validation') {
						sh "/usr/local/bin/sfdx force:source:deploy -c -w 50 ${LOCALRUNTESTS} -u ${env.ENVIROMENT}${env.PROJECT} -p force-app/main/default/ -x manifest/package.xml"
				}
			}
		}
		stage('Launching Testing') {
				sh "/usr/local/bin/sfdx force:apex:test:run -r json -d testing -c -u ${env.ENVIROMENT}${env.PROJECT} -w 50"
		}
		stage('Sending QA Results to Sonar') {
				// Give permissions to Jenkins User for access /bin/sonar-scanner
				sh "/bin/sonar-scanner -Dsonar.projectKey=${env.PROJECT} -Dsonar.sources=. -Dsonar.host.url=${SONARURL} -Dsonar.login=${SONARLOGIN} -Dsonar.apex.pmd.reportPaths=pmd_result.xml -Dsonar.apex.coverage.reportPath=testing"
		}  
		
	}
}
